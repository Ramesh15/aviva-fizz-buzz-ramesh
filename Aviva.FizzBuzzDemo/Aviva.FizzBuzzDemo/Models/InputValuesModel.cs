﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputValuesModel.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   The input values model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The input values model.
    /// </summary>
    public class InputValuesModel
    {
        /// <summary>
        /// Gets or sets the input number.
        /// </summary>
        [Required(ErrorMessage = "Please enter values")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "only integers allowed")]
        [Range(1, 1000, ErrorMessage = "Value should be between 1 and 1000")]
        [Display(Name = "Enter a number")]
        public int Number { get; set; }
    }
}