﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputValuesControllerTest.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the InputValuesControllerTest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.Tests.Controllers
{
    using System.Web.Mvc;
    using Aviva.FizzBuzzDemo.BusinessLogic;
    using Aviva.FizzBuzzDemo.Controllers;
    using Aviva.FizzBuzzDemo.Models;

    using Moq;

    using NUnit.Framework;
    using Assert = NUnit.Framework.Assert;

    /// <summary>
    /// The input values controller test.
    /// </summary>
    [TestFixture]
    class InputValuesControllerTest
    {
        /// <summary>
        /// The Instance of InputValuesController.
        /// </summary>
        private InputValuesController inputValuesController;

        /// <summary>
        /// The mock of FizzBuzzLogic.
        /// </summary>
        private Mock<IFizzBuzzLogic> mockFizzBuzzLogic;

        /// <summary>
        /// The Instance of InputValuesController.
        /// </summary>
        private InputValuesModel model;

        /// <summary>
        /// The initialize.
        /// </summary>
        [Test]
        public void Initialize()
        {
        }

        /// <summary>
        /// The index.
        /// </summary>
        [Test]
        public void Index()
        {
            // Arrange
            const string ExpectedViewName = "Index";
            var controller = new InputValuesController();

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result, "Should have returned a ViewResult");
            Assert.AreEqual(ExpectedViewName, result.ViewName, "View name should have been {0}", ExpectedViewName);
        }

        /// <summary>
        /// The Unit test case to test display fizz buzz list.
        /// </summary>
        [Test]
        public void DisplayFizzBuzzList()
        {
            // Arrange
            const string ExpectedViewName = "DisplayFizzBuzzList";
            var controller = new InputValuesController();
            this.model = new InputValuesModel();

            // Act
            var result = controller.DisplayFizzBuzzList(this.model) as ViewResult;

            // Assert
            Assert.IsNotNull(result, "Should have returned a ViewResult");
            Assert.AreEqual(ExpectedViewName, result.ViewName, "View name should have been {0}", ExpectedViewName);
        }
    }
}
