﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFizzBuzzPrinter.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the IFizzBuzzPrinter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    /// <summary>
    /// The FizzBuzzPrinter interface.
    /// </summary>
    public interface IFizzBuzzPrinter
    {
        /// <summary>
        /// The print.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        /// <returns>
        /// The string <see cref="string"/>.
        /// </returns>
        string Print(int number);
    }
}
