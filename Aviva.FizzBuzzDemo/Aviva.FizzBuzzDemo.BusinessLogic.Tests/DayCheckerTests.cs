﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DayCheckerTests.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the DayCheckerTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



namespace Aviva.FizzBuzzDemo.BusinessLogic.Tests
{
    using System;

    using Aviva.FizzBuzzDemo.BusinessLogic;

    using NUnit.Framework;

    /// <summary>
    /// The day checker tests.
    /// </summary>
    [TestFixture]
    public class DayCheckerTests
    {
        /// <summary>
        /// The interface for dayChecker.
        /// </summary>
        private IDayChecker dayChecker;

        /// <summary>
        /// The boolean to assign boolean of IsWednesday method.
        /// </summary>
        private bool isWednesday;

        /// <summary>
        /// The test initialize.
        /// </summary>
        [Test]
        public void TestInitialize()
        {
            // Arrange
            this.dayChecker = new DayChecker();
            this.isWednesday = default(bool);
        }

        /// <summary>
        /// The method to test day of the week is wednesday
        /// </summary>
        [Test]
        public void IsWednesdayTrue()
        {
            // Act
            string day = DateTime.Now.DayOfWeek.ToString();

            // Assert
            Assert.AreEqual("Wednesday", day);
        }

        /// <summary>
        /// The method to test day of the week is not wednesday.
        /// </summary>
        [Test]
        public void IsWednesdayFalse()
        {
            // Act
            string day = DateTime.Now.DayOfWeek.ToString();

            // Assert
            Assert.AreNotEqual("Wednesday", day);
        }
    }
}
