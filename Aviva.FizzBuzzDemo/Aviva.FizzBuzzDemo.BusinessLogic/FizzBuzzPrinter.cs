﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzPrinter.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   The fizz buzz printer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    using System;

    /// <summary>
    /// The fizz buzz printer.
    /// </summary>
    public class FizzBuzzPrinter : IFizzBuzzPrinter
    {
        /// <summary>
        /// The fizz checker.
        /// </summary>
        private IFizzChecker fizzChecker;

        /// <summary>
        /// The buzz checker.
        /// </summary>
        private IBuzzChecker buzzChecker;

        /// <summary>
        /// The day checker.
        /// </summary>
        private IDayChecker dayChecker;

        /// <summary>
        /// Initializes a new instance of the FizzBuzzPrinter class.
        /// </summary>
        /// <param name="fizzChecker">
        /// The fizz checker.
        /// </param>
        /// <param name="buzzChecker">
        /// The buzz checker.
        /// </param>
        /// <param name="dayChecker">
        /// The day checker.
        /// </param>
        public FizzBuzzPrinter(IFizzChecker fizzChecker, IBuzzChecker buzzChecker, IDayChecker dayChecker)
        {
            this.fizzChecker = fizzChecker;
            this.buzzChecker = buzzChecker;
            this.dayChecker = dayChecker;
        }

        ///// <summary>
        ///// Initializes a new instance of the <see cref="FizzBuzzPrinter"/> class.
        ///// </summary>
        public FizzBuzzPrinter()
            : this(new FizzChecker(), new BuzzChecker(), new DayChecker())
        {
        }

        /// <summary>
        /// The print.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        /// <returns>
        /// The string <see cref="string"/>.
        /// </returns>
        public string Print(int number)
        {
            if (this.fizzChecker.IsFizz(number) && this.buzzChecker.IsBuzz(number))
            {
                return this.dayChecker.IsWednesday() ? "WizzWuzz" : "FizzBuzz";
            }

            if (this.fizzChecker.IsFizz(number))
            {
                return this.dayChecker.IsWednesday() ? "Wizz" : "Fizz";
            }

            if (this.buzzChecker.IsBuzz(number))
            {
                return this.dayChecker.IsWednesday() ? "Wuzz" : "Buzz";
            }

            return Convert.ToString(number);
        }
    }
}
