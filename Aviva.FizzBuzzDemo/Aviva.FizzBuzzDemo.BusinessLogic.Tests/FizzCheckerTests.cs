﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzCheckerTests.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the FizzCheckerTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic.Tests
{
    using Aviva.FizzBuzzDemo.BusinessLogic;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// The fizz checker tests.
    /// </summary>
    [TestFixture]
    public class FizzCheckerTests
    {
        /// <summary>
        /// The interface fizzChecker.
        /// </summary>
        private IFizzChecker fizzChecker;

        /// <summary>
        /// The boolean to assign  isFizz.
        /// </summary>
        private bool isFizz;

        /// <summary>
        /// The mock fizz checker.
        /// </summary>
        private Mock<IFizzChecker> mockFizzChecker;

        /// <summary>
        /// The test initialize.
        /// </summary>
        [Test]
        public void TestInitialize()
        {
            // Arrange
            this.mockFizzChecker = new Mock<IFizzChecker>();
            this.isFizz = default(bool);
            this.fizzChecker = new FizzChecker();
        }

        /// <summary>
        /// Method to test the number is divisible by three
        /// </summary>
        [Test]
        public void WhenDivisibleByThreeReturnTrue()
        {
            // Act
            int number = 3;
            this.mockFizzChecker.Setup(c => c.IsFizz(number)).Returns(true);

            // Assert
            this.isFizz = this.fizzChecker.IsFizz(number);
            Assert.AreEqual(this.isFizz, true);
        }

        /// <summary>
        /// Method to test the number is not divisible by three
        /// </summary>
        [Test]
        public void WhenNotDivisibleByThreeReturnFalse()
        {
            // Act
            int number = 1;
            this.mockFizzChecker.Setup(c => c.IsFizz(number)).Returns(true);

            // Assert
            this.isFizz = this.fizzChecker.IsFizz(number);
            Assert.AreEqual(this.isFizz, false);
        }

        /// <summary>
        /// The when divide by zero return true.
        /// </summary>
        [Test]
        public void WhenDivideByZeroReturnTrue()
        {
            // Act
            int number = 0;
            this.mockFizzChecker.Setup(c => c.IsFizz(number)).Returns(true);

            // Assert
            this.isFizz = this.fizzChecker.IsFizz(number);
            Assert.AreEqual(this.isFizz, true);
        }
    }
}
