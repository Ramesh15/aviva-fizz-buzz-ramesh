﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzHelper.cs" company="Aviva">
//  Aviva
// </copyright>
// <summary>
//   The fizz buzz helper.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.Views
{
    /// <summary>
    /// The fizz buzz helper.
    /// </summary>
    public static class FizzBuzzHelper
    {
        /// <summary>
        /// The buzz class.
        /// </summary>
        public const string BuzzCssClass = "tile-green";

        /// <summary>
        /// The fizz class.
        /// </summary>
        public const string FizzCssClass = "tile-blue";

        /// <summary>
        /// The default class.
        /// </summary>
        public const string DefaultCssClass = "tile-default";

        /// <summary>
        /// To Get class for fizz buzz.
        /// </summary>
        /// <param name="item">
        /// The item.
        /// </param>
        /// <returns>
        /// The CSS Class<see cref="string"/>.
        /// </returns>
        public static string GetCssClassForFizzBuzz(string item)
        {
            if (item == "Buzz" || item == "Wuzz")
            {
                return BuzzCssClass;
            }

            if (item == "Fizz" || item == "Wizz")
            {
                return FizzCssClass;
            }

            return DefaultCssClass;
        }
    }
}