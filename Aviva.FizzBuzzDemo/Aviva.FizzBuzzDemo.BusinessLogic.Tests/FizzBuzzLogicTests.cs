﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzLogicTests.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the FizzBuzzLogicTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic.Tests
{
    using Aviva.FizzBuzzDemo.BusinessLogic;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// The fizz buzz logic tests.
    /// </summary>
    [TestFixture]
    public class FizzBuzzLogicTests
    {
        /// <summary>
        /// The fizz buzz logic.
        /// </summary>
        private IFizzBuzzLogic fizzBuzzLogiclogic;

        /// <summary>
        /// The mock FizzBuzz Printer
        /// </summary>
        private Mock<IFizzBuzzPrinter> mockFizzBuzzPrinter;

        /// <summary>
        /// The initialize.
        /// </summary>
        [Test]
        public void Initialize()
        {
            this.mockFizzBuzzPrinter = new Mock<IFizzBuzzPrinter>();
            this.fizzBuzzLogiclogic = new FizzBuzzLogic(this.mockFizzBuzzPrinter.Object);
        }
    }
}
