﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzLogic.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the FizzBuzzLogic type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    using System.Collections.Generic;

    /// <summary>
    /// The fizz buzz logic.
    /// </summary>
    public class FizzBuzzLogic : IFizzBuzzLogic
    {
        /// <summary>
        /// The list string.
        /// </summary>
        private List<string> lstString;

        /// <summary>
        /// The fizz buzz printer.
        /// </summary>
        private IFizzBuzzPrinter fizzBuzzPrinter;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzLogic"/> class.
        /// </summary>
        /// <param name="printer">
        /// The printer.
        /// </param>
        public FizzBuzzLogic(IFizzBuzzPrinter printer)
        {
            this.fizzBuzzPrinter = printer;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzLogic"/> class.
        /// </summary>
        public FizzBuzzLogic()
        {
            //throw new System.NotImplementedException();
        }

        /// <summary>
        /// The get integers list with in start and end number.
        /// </summary>
        /// <param name="startNumber">
        /// The start Number.
        /// </param>
        /// <param name="endNumber">
        /// The end Number.
        /// </param>
        /// <returns>
        /// The List of strings.
        /// </returns>
        public List<string> GetIntegersList(int startNumber, int endNumber)
        {
            this.lstString = new List<string>();

            for (int number = startNumber; number <= endNumber; number++)
            {
                this.lstString.Add(this.fizzBuzzPrinter.Print(number));
            }

            return this.lstString;
        }
    }
}
