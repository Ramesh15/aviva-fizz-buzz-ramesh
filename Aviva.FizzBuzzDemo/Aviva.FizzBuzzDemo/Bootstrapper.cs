using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc3;

namespace Aviva.FizzBuzzDemo
{
    using Aviva.FizzBuzzDemo.BusinessLogic;

    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();      

            container.RegisterType<IFizzBuzzLogic, FizzBuzzLogic>();
            container.RegisterType<IFizzBuzzPrinter, FizzBuzzPrinter>();
            container.RegisterType<IFizzChecker, FizzChecker>();
            container.RegisterType<IBuzzChecker, BuzzChecker>();
            container.RegisterType<IDayChecker, DayChecker>();

            return container;
        }
    }
}