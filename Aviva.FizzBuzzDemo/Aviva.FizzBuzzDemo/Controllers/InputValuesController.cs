﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InputValuesController.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the InputValuesController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.Controllers
{
    using System.Web.Mvc;
    using Aviva.FizzBuzzDemo.BusinessLogic;
    using Aviva.FizzBuzzDemo.Models;

    /// <summary>
    /// The input values controller.
    /// </summary>
    public class InputValuesController : Controller
    {
        /// <summary>
        /// The display list Model.
        /// </summary>
        private readonly DisplayListModel displayListModel = new DisplayListModel();

        /// <summary>
        /// The Interface for fizzbuzzlogic
        /// </summary>
        private readonly IFizzBuzzLogic fizzbuzzlogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="InputValuesController"/> class.
        /// </summary>
        /// <param name="fizzbuzzlogic">
        /// The fizzbuzzlogic
        /// </param>
        public InputValuesController(IFizzBuzzLogic fizzbuzzlogic)
        {
            this.fizzbuzzlogic = fizzbuzzlogic;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InputValuesController"/> class.
        /// </summary>
        public InputValuesController() : this(new FizzBuzzLogic())
        {
        }

        /// <summary>
        /// The index.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            return View("Index");
        }

        /// <summary>
        /// The display fizz buzz list.
        /// </summary>
        /// <param name="model">
        /// The model.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult DisplayFizzBuzzList(InputValuesModel model)
        {
            this.displayListModel.ListValues = this.fizzbuzzlogic.GetIntegersList(1, model.Number);
            return View("DisplayFizzBuzzList", this.displayListModel);
        }

    }
}
