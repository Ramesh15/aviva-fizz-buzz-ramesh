﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFizzBuzzLogic.cs" company="Aviva">
//  Aviva 
// </copyright>
// <summary>
//   Defines the IFizzBuzzLogic type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    using System.Collections.Generic;

    /// <summary>
    /// The FizzBuzzLogic interface.
    /// </summary>
    public interface IFizzBuzzLogic
    {
        /// <summary>
        /// The get integers list.
        /// </summary>
        /// <param name="startNumber">
        /// The start number.
        /// </param>
        /// <param name="endNumber">
        /// The end number.
        /// </param>
        /// <returns>
        /// The <see cref="List{T}"/>.
        /// </returns>
        List<string> GetIntegersList(int startNumber, int endNumber);
    }
}
