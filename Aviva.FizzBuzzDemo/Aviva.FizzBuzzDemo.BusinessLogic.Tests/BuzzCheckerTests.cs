﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BuzzCheckerTests.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the BuzzCheckerTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic.Tests
{
    using Aviva.FizzBuzzDemo.BusinessLogic;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// The buzz checker tests.
    /// </summary>
    [TestFixture]
    public class BuzzCheckerTests
    {

        /// <summary>
        /// The Interface buzzChecker
        /// </summary>
        private IBuzzChecker buzzChecker;

        /// <summary>
        /// The boolean to assign isFizz.
        /// </summary>
        private bool isBuzz;

        /// <summary>
        /// The mock buzz checker.
        /// </summary>
        private Mock<IBuzzChecker> mockBuzzChecker;

        /// <summary>
        /// The test initialize.
        /// </summary>
        [Test]
        public void TestInitialize()
        {
            // Arrange
            this.mockBuzzChecker = new Mock<IBuzzChecker>();
            this.isBuzz = default(bool);
            this.buzzChecker = new BuzzChecker();
        }

        /// <summary>
        /// Method to test number is divisible by five
        /// </summary>
        [Test]
        public void WhenDevisibleByFiveReturnTrue()
        {
            // Act
            int number = 5;
            this.mockBuzzChecker.Setup(c => c.IsBuzz(number)).Returns(true);

            // Assert
            this.isBuzz = this.buzzChecker.IsBuzz(number);
            Assert.AreEqual(this.isBuzz, true);
        }

        /// <summary>
        /// Method to test number is not divisible by five
        /// </summary>
        [Test]
        public void WhenNotDevisibleByFiveReturnFalse()
        {
            // Act
            int number = 7;
            this.mockBuzzChecker.Setup(c => c.IsBuzz(number)).Returns(true);

            // Assert
            this.isBuzz = this.buzzChecker.IsBuzz(number);
            Assert.AreEqual(this.isBuzz, false);
        }

        /// <summary>
        /// Method to test when number divide by zero.
        /// </summary>
        [Test]
        public void WhenDevideByZeroReturnTrue()
        {
            // Act
            int number = 0;
            this.mockBuzzChecker.Setup(c => c.IsBuzz(number)).Returns(true);

            // Assert
            this.isBuzz = this.buzzChecker.IsBuzz(number);
            Assert.AreEqual(this.isBuzz, true);
        }
    }
}
