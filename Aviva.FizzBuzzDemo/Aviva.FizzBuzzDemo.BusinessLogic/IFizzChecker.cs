﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IFizzChecker.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   The FizzChecker interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    /// <summary>
    /// The FizzChecker interface.
    /// </summary>
    public interface IFizzChecker
    {
        /// <summary>
        /// The IsFizz method.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        /// <returns>
        /// The boolean <see cref="bool"/>.
        /// </returns>
        bool IsFizz(int number);
    }
}
