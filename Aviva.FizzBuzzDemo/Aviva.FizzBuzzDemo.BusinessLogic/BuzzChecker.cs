﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BuzzChecker.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   The buzz checker class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    /// <summary>
    /// The buzz checker class.
    /// </summary>
    public class BuzzChecker : IBuzzChecker
    {
        /// <summary>
        /// The is buzz.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        /// <returns>
        /// The boolean<see cref="bool"/>.
        /// </returns>
        public bool IsBuzz(int number)
        {
            return (number % 5) == 0;
        }
    }
}
