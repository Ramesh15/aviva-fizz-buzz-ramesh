﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IDayChecker.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the IDayChecker type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    /// <summary>
    /// The DayChecker interface.
    /// </summary>
    public interface IDayChecker
    {
        /// <summary>
        /// The is wednesday.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsWednesday();
    }
}
