﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FizzBuzzPrinterTests.cs" company="Aviva">
//   Aviva
// </copyright>
// <summary>
//   Defines the FizzBuzzPrinterTests type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva.FizzBuzzDemo.BusinessLogic.Tests
{
    using Aviva.FizzBuzzDemo.BusinessLogic;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// The fizz buzz printer tests.
    /// </summary>
    [TestFixture]
    public class FizzBuzzPrinterTests
    {
        /// <summary>
        /// The interface fizz buzz printer.
        /// </summary>
        private IFizzBuzzPrinter fizzBuzzPrinter;

        /// <summary>
        /// The mock fizz checker.
        /// </summary>
        private Mock<IFizzChecker> mockFizzChecker;

        /// <summary>
        /// The mock buzz checker.
        /// </summary>
        private Mock<IBuzzChecker> mockBuzzChecker;

        /// <summary>
        /// The mock day checker.
        /// </summary>
        private Mock<IDayChecker> mockDayChecker;

        /// <summary>
        /// The mock day checker.
        /// </summary>
        private Mock<IFizzBuzzPrinter> mockFizzBuzzPrinter;

        /// <summary>
        /// The initialize.
        /// </summary>
        [Test]
        public void Initialize()
        {
            this.mockFizzChecker = new Mock<IFizzChecker>();
            this.mockBuzzChecker = new Mock<IBuzzChecker>();
            this.mockDayChecker = new Mock<IDayChecker>();
            this.mockFizzBuzzPrinter = new Mock<IFizzBuzzPrinter>();
            this.fizzBuzzPrinter = new FizzBuzzPrinter(
                this.mockFizzChecker.Object,
                this.mockBuzzChecker.Object,
                this.mockDayChecker.Object);
        }

        /// <summary>
        /// Method to test when number is in fizz checker condition only return fizz.
        /// </summary>
        [Test]
        public void WhenNumberIsInFizzCheckerConditionOnlyReturnFizz()
        {
            // Arrange
            int number = 3;
            this.mockFizzChecker.Setup(checker => checker.IsFizz(number)).Returns(true);
            this.mockBuzzChecker.Setup(checker => checker.IsBuzz(number)).Returns(false);

            // Act
            string result = this.fizzBuzzPrinter.Print(number);
            Assert.AreEqual("Fizz", result);
        }

        /// <summary>
        /// Method to test when number is in buzz checker condition only return fizz.
        /// </summary>
        [Test]
        public void WhenNumberIsInBuzzCheckerConditionOnlyReturnBuzz()
        {
            // Arrange
            int number = 5;
            this.mockFizzChecker.Setup(checker => checker.IsFizz(number)).Returns(false);
            this.mockBuzzChecker.Setup(checker => checker.IsBuzz(number)).Returns(true);

            // Act
            string result = this.fizzBuzzPrinter.Print(number);
            Assert.AreEqual("Buzz", result);
        }

        /// <summary>
        /// Method to test when number is in both buzz checker and fizz checker condition return fizz buzz.
        /// </summary>
        [Test]
        public void WhenNumberIsInBothBuzzCheckerAndFizzCheckerConditionReturnFizzBuzz()
        {
            // Arrange
            int number = 15;
            this.mockFizzChecker.Setup(checker => checker.IsFizz(number)).Returns(true);
            this.mockBuzzChecker.Setup(checker => checker.IsBuzz(number)).Returns(true);

            // Act
            string result = this.fizzBuzzPrinter.Print(number);
            Assert.AreEqual("FizzBuzz", result);
        }
    }
}
