﻿namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    using System;

    public class DayChecker : IDayChecker
    {
        /// <summary>
        /// The is wednesday.
        /// </summary>
        /// <returns>
        /// The boolean <see cref="bool"/>.
        /// </returns>
        public bool IsWednesday()
        {
            return DateTime.Now.DayOfWeek.ToString() == string.Format("Wednesday");
        }
    }
}
