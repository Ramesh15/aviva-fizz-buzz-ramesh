﻿namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    public interface IBuzzChecker
    {
        /// <summary>
        /// The IsBuzz method.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        /// <returns>
        /// The method returns boolean <see cref="bool"/>.
        /// </returns>
        bool IsBuzz(int number);
    }
}
