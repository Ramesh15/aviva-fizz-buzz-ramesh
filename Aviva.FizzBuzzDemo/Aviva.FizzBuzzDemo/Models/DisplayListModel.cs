﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aviva.FizzBuzzDemo.Models
{
    public class DisplayListModel
    {
        /// <summary>
        /// Gets or sets the list values.
        /// </summary>
        public List<string> ListValues { get; set; }
    }
}