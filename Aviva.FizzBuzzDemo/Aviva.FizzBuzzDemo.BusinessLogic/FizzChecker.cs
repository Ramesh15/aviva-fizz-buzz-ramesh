﻿namespace Aviva.FizzBuzzDemo.BusinessLogic
{
    public class FizzChecker : IFizzChecker
    {
        /// <summary>
        /// The fizz checker.
        /// </summary>
        private IFizzChecker fizzChecker;
        
        /// <summary>
        /// The is fizz.
        /// </summary>
        /// <param name="number">
        /// The number.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsFizz(int number)
        {
            return (number % 3) == 0;
        }
    }
}
